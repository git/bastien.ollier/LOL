# Projet d'Entity FrameWork et Consomation et Développement de services sur League Of Legends

Ce projet est un travail universitaire réalisé durant la deuxième année de BUT à Clermont-Ferrand. Il correspond au travail demandé dans le cadre du cours regroupant Entity Framework et Consommation et Developpement de Services.

![League Of Legends](./Docs/imageCodeFirst/frontImageReadMe.jpg)

Explique ce qu'on a fait et ce qu'on a pas fait et pourquoi on a priorisé ca plutot que d'autre


# :zap: Consommation  et Développement de services

Voici l'état des différentes tâches liées à la consommation et au développement de services :


> * :white_check_mark: La mise en place de toutes les opérations CRUD est terminée.
> * :white_check_mark: Une API RESTful a été mise en place en respectant les règles de routage et en utilisant les bons codes de statut.
> * :white_check_mark: La version de l'API a été gérée de manière appropriée.
> * :white_check_mark: Les logs ont été implémentés.
> * :construction: Les tests unitaires sont en cours de réalisation.
> * :construction: La création du client MAUI et sa liaison avec l'API sont en cours de réalisation.
> * :white_check_mark: La liaison avec la base de données est opérationnelle.
> * :white_check_mark: Le filtrage et la pagination des données ont été implémentés.
> * :white_check_mark: Le code est de qualité grâce à l'utilisation de SonarQube.
> * :white_check_mark: L'API a été dockerisée et hébergée sur CodeFirst.
> * :construction: Sécurité

`Note : Le client MAUI n'a pas été réalisé par manque de temps. Les tests unitaires et la sécurité sont réalisés au fur et à mesure de l'avancement du projet.`


---


# :zap: Entity Framework :

Voici l'état des différentes tâches liées à Entity Framework :

> * :white_check_mark: **Exercice 1** : Une base de données a été créée avec une table pour les champions, et des requêtes CRUD ont été implémentées, ainsi que du filtrage et de la pagination. Le client console n'a pas été réalisé pour cet exercice par manque de temps.
> * :white_check_mark: **Exercice 2** : Des tests unitaires ont été écrits et une base de données a été simulée à l'aide de SQLiteInMemory.
> * :white_check_mark: **Exercice 3** : Entity Framework a été déployé et les tests ont été effectués via Code#0.
> * :white_check_mark: **Exercice 4**: Les tables pour les runes et les skins ont été implémentées (sans les relations).
> * :white_check_mark: **Exercice 5** : Une relation OneToMany a été établie entre les champions et les skins.
> * :white_check_mark: **Exercice 6** : Une relation ManyToMany a été établie entre les champions, les rune pages et les runes.
> * :construction: **Exercice 7** : Le mapping entre le modèle et l'entité a été réalisé pour améliorer la qualité du code.
> * :construction: **Exercice 8** : La mise en place du pattern UnitOfWork n'a pas pu être implémentée par manque de temps.
--- 


### Diagramme d'architechture :

![Diagramme d'architechture](./Docs/imageCodeFirst/DiagrammeArchitecture.png)


#### Partie Client :

La partie client qui n'a pas pu être réalisé dans notre cas, est sensée être constituée du client MAUI et du client Console, qui affichent les ressources et testent l'architecture en utilisant le HTTPDataManager pour effectuer des requêtes à l'API et récupérer des données.

#### DataManager :

Le DataManager utilise l'une des extensions mapper pour convertir les objets DTO en Model, et peut être remplacé par EFDataManager ou StubLib.

#### Partie API :

La partie API reçoit les requêtes et renvoie les objets en conséquence, en utilisant l'EFDataManager pour accéder aux données stockées en base de données. La fluent API permet de définir précisément les attributs de la base de données.

#### Entity Framework :
L'EntityFramework est implémenté avec toutes les classes Entity dérivant du modèle, en utilisant OneToMany et ManyToMany de manière dérivée de celle prévue. Les méthodes CRUD sont implémentées grâce à l'utilisation de l'EFDataManager et le Mapper entre entity et model est requis.

#### Déploiement :
Le projet est déployé avec le projet en conteneur, mais le pattern UnitOfWork n'a pas été abordé.

# :tada: Comment lancer le projet 

## 1 - Cloner le dépot 

Cloner le dépôt Git en utilisant la commande suivante :

    git clone https://codefirst.iut.uca.fr/git/bastien.ollier/LOL.git

## 2 - Configurer le démarrage du projet 

> Configurer le projet de démarrage en cliquant sur "Projet de démarrage" à gauche de la flèche verte, puis en sélectionnant l'option "apiLOL". 

## 3 - Lancement du projet

Le projet est maintenant prêt à être lancé. Vous pouvez commencer à faire des requêtes sur la base de données via l'API.