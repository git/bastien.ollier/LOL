﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace EFLol
{
	public class ChampionEntity
	{
		public int Id { get; set; }

		[MaxLength(30, ErrorMessage = "Name cannot be longer than 30 characters.")]
		public string Name { get; set; }

		[MaxLength(256, ErrorMessage = "Bio cannot be longer than 256 characters.")]
		public string Bio { get; set; }

		//public ChampionClass Class { get; set; }
		//public string Icon { get; set; }
		//public LargeImage Image { get; set; }
		//public ReadOnlyDictionary<string, int> Characteristics { get; private set; }



		public HashSet<SkillEntity> skills = new HashSet<SkillEntity>();
		public Collection<SkinEntity> Skins { get; set; }
		public Collection<RunePageEntity> ListRunePages { get; set; }
}
}
