﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace EFLol.DBDataManager
{
	public static class DataConverter
	{
		public static Champion ChampionToPoco(this ChampionEntity champion)
		{
			return new Champion(champion.Name, champion.Bio);
		}

		public static ChampionEntity ChampionToEntity(this Champion champion)
		{
			return new ChampionEntity
			{
				Name = champion.Name,
				Bio = champion.Bio
			};
		}
	}
}
