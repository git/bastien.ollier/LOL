﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EFLol
{
	public class MyDbContext : DbContext
	{
		public DbSet<ChampionEntity> Champions { get; set; }
		public DbSet<SkinEntity> Skins { get; set; }
        public DbSet<SkillEntity> Skill { get; set; }
        public DbSet<RunePageEntity> RunePages { get; set; }
        public DbSet<RuneEntity> Runes { get; set; }

        public MyDbContext()
		{ }

		public MyDbContext(DbContextOptions<MyDbContext> options)
			: base(options)
		{ }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				optionsBuilder.UseSqlite("Data Source=../EFLol/loldb.db");
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Entity<ChampionEntity>().Property(c => c.Id).ValueGeneratedOnAdd();
			modelBuilder.Entity<SkinEntity>().Property(s => s.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<SkillEntity>().Property(s => s.Id).ValueGeneratedOnAdd();

			modelBuilder.Entity<ChampionEntity>().HasMany(p => p.skills).WithMany(p => p.champions).UsingEntity(j => j.ToTable("ChampionSkills"));

            modelBuilder.Entity<RunePageEntity>().Property(s => s.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<RuneEntity>().Property(s => s.Id).ValueGeneratedOnAdd();
        }
    }
}
