﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using EFLol;


internal class Program
{
    private static void Main(string[] args)
    {
        using (var context = new MyDbContext())
        {
            // Clean the DB before starting
            Console.WriteLine("Clean the DB before starting");
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }


        SkinEntity black = new SkinEntity { Name = "Black", Description = "Black skin", Icon = "black.png", Price = 0.99f };
        SkinEntity white = new SkinEntity { Name = "White", Description = "White skin", Icon = "white.png", Price = 150.99f };
        SkinEntity green = new SkinEntity { Name = "Green", Description = "Green skin", Icon = "green.png", Price = 4.99f };


        RuneEntity rune1 = new RuneEntity { Id = 1, Name = "rune1", Description = "super rune1" };
        RuneEntity rune2 = new RuneEntity { Id = 2, Name = "rune2", Description = "super rune2" };
        RuneEntity rune3 = new RuneEntity { Id = 3, Name = "rune3", Description = "super rune3" };

        RunePageEntity runePage1 = new RunePageEntity { Id = 1, Name = "runepage1"/*, Runes = new Dictionary<Category, RuneEntity> { { Category.Major, rune1 } }*/ };

        ChampionEntity Zeus = new ChampionEntity { Name = "Zeus", Bio = "Zeus is the god of the sky", Skins = new Collection<SkinEntity>(new List<SkinEntity> { black, white }) };
        ChampionEntity Hera = new ChampionEntity { Name = "Hera", Bio = "Hera is the goddess of marriage", Skins = new Collection<SkinEntity>(new List<SkinEntity> { green }) };
        ChampionEntity Poseidon = new ChampionEntity {
            Name = "Poseidon", Bio = "Poseidon is the god of the sea",
            ListRunePages = new Collection<RunePageEntity>(new List<RunePageEntity> { { runePage1 } })
        };

        SkillEntity skill1 = new SkillEntity
        {
            Id = 1,
            Name = "skill1",
            Description = "super skill",
            SkillType = SkillTypeEntity.Basic,
            champions = new HashSet<ChampionEntity> { Zeus, Hera }
        };

        SkillEntity skill2 = new SkillEntity
        {
            Id = 2,
            Name = "skill2",
            Description = "super skill",
            SkillType = SkillTypeEntity.Basic,
            champions = new HashSet<ChampionEntity> { Zeus}
        };

        Zeus.skills.Add(skill1);
        Zeus.skills.Add(skill2);

        Hera.skills.Add(skill1);

        using (var context = new MyDbContext())
        {
            Console.WriteLine("Creates and inserts new Champions");
            context.Add(Zeus);
            context.Add(Hera);
            context.Add(Poseidon);

            context.Add(skill1);
            context.Add(skill2);
            context.SaveChanges();
        }


        using (var context = new MyDbContext())
        {
            foreach (var n in context.Champions)
            {
                // LINQ to select the skins of the current champion
                var skins = context.Champions.Where(c => c.Id == n.Id).SelectMany(c => c.Skins);
                var runePage = context.Champions.Where(c => c.Id == n.Id).SelectMany(c => c.ListRunePages);

                // Display the champion and its skins
                Console.WriteLine($"Champion n°{n.Id} - {n.Name} : {skins.Count()} skins, {runePage.Count()} runePage");
            }
            context.SaveChanges();
        }

        using (var context = new MyDbContext())
        {
            foreach (var n in context.Skins)
            {
                Console.WriteLine($"Skin n°{n.Id} - {n.Name}" + (n.Price != null ? $" - {n.Price}" : ""));
            }
            context.SaveChanges();
        }

        using (var context = new MyDbContext())
        {
            foreach (var n in context.RunePages)
            {
                Console.WriteLine($"runePage n°{n.Id} - {n.Name}");
            }
            context.SaveChanges();
        }
    }
}