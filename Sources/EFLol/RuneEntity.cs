﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFLol
{
	public class RuneEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public RuneFamilyEntity RuneFamily { get; set; }
        public Collection<RunePageEntity> ListRunePages { get; set; }
    }

    public enum RuneFamilyEntity
    {
        Unknown,
        Precision,
        Domination
    }

}

