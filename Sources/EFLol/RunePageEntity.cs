﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFLol
{
	public class RunePageEntity
	{
        public int Id { get; set; }
        public String Name { get; set; }
		//public Dictionary<Category, RuneEntity> Runes { get; set; }
	}

	public enum Category
	{
		Major,
		Minor1,
		Minor2,
		Minor3,
		OtherMinor1,
		OtherMinor2
	}
}
