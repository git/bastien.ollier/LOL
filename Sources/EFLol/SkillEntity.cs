﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFLol
{
	public class SkillEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public SkillTypeEntity SkillType { get; set; }
        public HashSet<ChampionEntity> champions { get; set; }

    }

    public enum SkillTypeEntity
	{
		Unknown,
		Basic,
		Passive,
		Ultimate
	}
}
