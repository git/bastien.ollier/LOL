using apiLOL.Controllers;
using apiLOL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using StubLib;

namespace TestUnitaire
{
	public class TestAPILol
	{
		[Theory]
		[InlineData("Beatrice", "sdfsdfd", "icon.png")]
		[InlineData("Maurice", "Ahri est un champion de League of Legends", "icon.png")]
		[InlineData("Loupiotte", "Akali est un champion de League of Legends", "icon.png")]
		public async Task TestPostChampion(string name, string bio, string icon)
		{
			// Arrange
			var data = new StubData();
			var logger = new NullLogger<ControllerChampions>();
			var controller = new ControllerChampions(data, logger);
			var champDTO = new ChampionDTO(name, bio, icon);

			// Act
			var nbInListBefore = data.ChampionsMgr.GetNbItems().Result;
			var result = await controller.AddChampion(champDTO);
			var nbInListAfter = data.ChampionsMgr.GetNbItems().Result;

			// Assert
			// IS the champion added to the list, number of champions in the list + 1
			Assert.Equal(nbInListBefore + 1, nbInListAfter);
			// Test le code de retour
		}


		[Theory]
		[InlineData("Beatrice", "Aatrox est un champion de League of Legends", "icon.png")]
		[InlineData("Maurice", "Ahri est un champion de League of Legends", "icon.png")]
		[InlineData("Loupiotte", "Akali est un champion de League of Legends", "icon.png")]
		public async Task TestGetChampion(string name, string bio, string icon)
		{
			// Arrange
			var data = new StubData();
			var logger = new NullLogger<ControllerChampions>();
			var controller = new ControllerChampions(data, logger);
			var champDTO = new ChampionDTO(name, bio, icon);

			// Act
			// Call method POST to add a champion
			var result = await controller.AddChampion(champDTO);
			// Call method GET to get the champion
			var resultGet = await controller.GetChampionByName(name);

			// Assert
			Assert.Equal(name, champDTO.Name);
			Assert.Equal(bio, champDTO.Bio);
		}

		[Theory]
		[InlineData("Beatrice", "Nouvelle bio")]
		[InlineData("Maurice", "Nouvelle bio")]
		[InlineData("Loupiotte", "Nouvelle bio")]
		public async Task TestPutChampion(string name, string bio)
		{
			// Method that change the bio of a champion. Make a test to check if the bio is changed
			// Arrange
			var data = new StubData();
			var logger = new NullLogger<ControllerChampions>();
			var controller = new ControllerChampions(data, logger);

			// Act
			// Add a champion
			var champDTO = new ChampionDTO(name, "Ancienne bio", "icon.png");
			var resultPost = await controller.AddChampion(champDTO);

			// Call method PUT to change the bio of a champion
			var resultPut = await controller.UpdateChampion(name, bio);
			var champion = (await data.ChampionsMgr.GetItemsByName(name, 0, 1)).First();
			var bioOfChampion = champion.Bio;

			// Assert
			// Does the bio of the champion is changed
			Assert.Equal(bio, bioOfChampion);
		}


		[Theory]
		[InlineData("Beatrice")]
		[InlineData("Maurice")]
		[InlineData("Loupiotte")]
		public async Task TestDeleteChampion(string name)
		{
			// Method that delete a champion. Make a test to check if the champion is deleted
			// Arrange
			var data = new StubData();
			var logger = new NullLogger<ControllerChampions>();
			var controller = new ControllerChampions(data, logger);

			// Act
			// Add a champion
			var champDTO = new ChampionDTO(name, "Ancienne bio", "icon.png");
			var resultPost = await controller.AddChampion(champDTO);

			// Call method DELETE to delete the champion
			var resultDelete = await controller.DeleteChampion(name);

			// Assert
			// Does the type of the result is a OkObjectResult
			Assert.IsType<OkObjectResult>(resultDelete);
		}
	}
}