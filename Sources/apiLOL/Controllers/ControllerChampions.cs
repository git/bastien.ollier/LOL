﻿using System.Security.Cryptography;
using apiLOL.DTO;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace apiLOL.Controllers
{
	[ApiController]
	[Route("api/v1/[controller]")]
	[ApiVersion("1.0")]

	public class ControllerChampions : Controller
	{
		//private readonly IDataManager data;
		private IChampionsManager _dataManager;
		private readonly ILogger _logger;

		public ControllerChampions(IDataManager manager, ILogger<ControllerChampions> log)
		{
			_dataManager = manager.ChampionsMgr;
			_logger = log;
		}


		[HttpGet]
		[ProducesResponseType(typeof(ChampionPageDTO), 200)]
		public async Task<IActionResult> GetChampions([FromQuery] int index = 0, int count = 10, string? name = "")
		{
			_logger.LogInformation($"methode Get de ControllerChampions appelée");
			int nbChampions = await _dataManager.GetNbItems();
			_logger.LogInformation($"Nombre de champions : {nbChampions}");

			var champs = (await _dataManager.GetItemsByName(name, index, int.MaxValue))
				.Where(champ => string.IsNullOrEmpty(name) || champ.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase))
				.Take(count)
				.Select(Model => Model.ToDTO());

			var page = new ChampionPageDTO
			{
				Data = champs,
				Index = index,
				Count = champs.Count(),
				TotalCount = nbChampions
			};
			return Ok(page);
		}

		[HttpGet]
		[Route("{name}")]
		[ProducesResponseType(typeof(ChampionDTO), 200)]
		public async Task<IActionResult> GetChampionByName(string name)
		{
			_logger.LogInformation($"methode GetChampion de ControllerChampions appelée avec le paramètre {name}");
			try
			{
				var champs = await _dataManager.GetItemsByName(name, 0, 1);
				if (champs.Any())
				{
					return Ok(champs.First().ToDTO());
				}
				else
				{
					return NotFound();
				}
			}
			catch (Exception ex)
			{
				_logger.LogError($"erreur methode Get de ControllerChampions: {ex}");
				return BadRequest("erreur de nom de champion");
			}
			
		}


		[HttpPost]
		public async Task<IActionResult> AddChampion(ChampionDTO champDTO)
		{
			_logger.LogInformation($"methode Post de ControllerChampions appelée avec le paramètre {champDTO.Name}");
			try
			{
				// Check if the champion already exists in the database
				var champs = await _dataManager.GetItemsByName(champDTO.Name, 0, 1);
				if (champs.Any())
				{
					return BadRequest("le champion existe deja");
				}

				Champion tmp = champDTO.ToModel();
				Champion champion = await _dataManager.AddItem(tmp);
				ChampionDTO dtoChamp = champion.ToDTO();
				return CreatedAtAction(nameof(GetChampionByName), new { name = dtoChamp.Name }, dtoChamp);
			}
			catch (Exception ex)
			{
				_logger.LogError($"erreur methode Post de ControllerChampions: {ex}");
				return BadRequest("erreur lors de l'ajout du champion");
			}
		}


		[HttpPut("{name}")]
		public async Task<IActionResult> UpdateChampion(string name, string bio)
		{
			_logger.LogInformation(
				$"methode Put de ControllerChampions appelée avec le paramètre name: {name} et bio: {bio}");

			try
			{
				var champs = (await _dataManager.GetItemsByName(name, 0, 1)).First();
				champs.Bio = bio;
				return Ok(champs.ToDTO());
			}
			catch (Exception ex)
			{
				_logger.LogError($"erreur methode Put de ControllerChampions: {ex}");
				return BadRequest("erreur de nom de champion");
			}
		}

		[HttpDelete("{name}")]
		public async Task<IActionResult> DeleteChampion(string name)
		{
			_logger.LogInformation($"methode Delete de ControllerChampions appelée avec le paramètre name: {name}");

			try
			{
				var champ = (await _dataManager.GetItemsByName(name, 0, 1)).First();
				_dataManager.DeleteItem(champ);
				return Ok(champ.ToDTO());
			}
			catch (Exception ex)
			{
				_logger.LogError($"erreur methode Delete de ControllerChampions: {ex}");
				return BadRequest("erreur de nom de champion");
			}
		}

	}

	[ApiController]
	[Route("api/v2/[controller]")]
	[ApiVersion("2.0")]

	public class ControllerChampionsSecondVersion : Controller
	{
		private readonly ILogger _logger;

		public ControllerChampionsSecondVersion(ILogger<ControllerChampions> log)
		{
			_logger = log;
		}


		[HttpGet()]
		public string Get(int id)
		{
			return "Version 2 of GET";
		}

		[HttpPost]
		public void Post([FromBody] string value)
		{
		}

		[HttpPut("{id}")]
		public void Put(int id, [FromBody] string value)
		{
		}

		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}

	}
}