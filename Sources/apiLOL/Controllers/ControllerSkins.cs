﻿using apiLOL.DTO;
using Microsoft.AspNetCore.Mvc;
using Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiLOL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ControllerSkins : ControllerBase
    {
        private readonly IDataManager data;
        // EFdata manager qui implémente l'interface IDataManager
        // coté client : Refaire un APIdata manager qui implémente l'interface IDataManager
        private readonly ILogger _logger;

        public ControllerSkins(IDataManager manager, ILogger<ControllerChampions> log)
        {
            data = manager;
            _logger = log;
        }


        // GET api/<ControllerSkins>/5
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int index = 0, int count = 10, string? name = "")
        {
            _logger.LogInformation($"methode Get de ControllerSkins appelée index:{index}, count: {count} et name:{name}");
            int nbSkins = await data.SkinsMgr.GetNbItems();
            _logger.LogInformation($"Nombre de skins : {nbSkins}");

            var skin = (await data.SkinsMgr.GetItems(index, await data.SkinsMgr.GetNbItems())).Select(Model => Model.ToDTO());//.Where(Model => Model.Name.Contains(name)).Skip(index * count).Take(count).Select(Model => Model.ToDTO());

            var page = new SkinPageDTO
            {
                Data = (IEnumerable<SkinDTO>)skin,
                Index = index,
                Count = count,
                TotalCount = nbSkins
            };
            return Ok(page);
        }

        // POST api/<ControllerSkins>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ControllerSkins>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ControllerSkins>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
