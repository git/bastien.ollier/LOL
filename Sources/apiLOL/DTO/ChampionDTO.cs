﻿namespace apiLOL.DTO
{
    public class ChampionDTO
    {

		public ChampionDTO(string name, string bio, string icon)
		{
            Name = name;
            Bio = bio;
			Icon = icon;
		}

        public string Name { get; set; }
        public string Bio { get; set; }
		public string Icon { get; set; }

		public bool Equals(ChampionDTO other) => other.Name == this.Name && other.Bio == this.Bio && other.Icon == this.Icon;

		public string toString() => $"ChampionDTO: {Name} {Bio} {Icon}";
	}
}
