﻿using Model;

namespace apiLOL.DTO
{
    public static class ChampionMapper
    {
        public static ChampionDTO ToDTO(this Champion champion) 
            => new ChampionDTO(champion.Name, champion.Bio, champion.Icon);


        public static Champion ToModel(this ChampionDTO championDTO)
        {
            Champion champ = new Champion(championDTO.Name);
            champ.Bio = championDTO.Bio;
			champ.Icon = championDTO.Icon;
			return champ;
        }

    }
}
