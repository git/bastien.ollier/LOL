﻿using Model;

namespace apiLOL.DTO
{
    public class SkinDTO
    {
        public SkinDTO(string name, float price = 0.0f, string icon = "", string image = "", string description = "")
        {
            Name = name;
            Price = price;
            Icon = icon;
            Description = description;
        }

        public string Name { get; set; }
        public float Price { get; }
        public string Icon { get; }
        public string Description { get; }
    }
}
