﻿using Model;

namespace apiLOL.DTO
{
    public static class SkinMapper
    {
       public static SkinDTO ToDTO(this Skin skin)
            => new SkinDTO(skin.Name, skin.Price, skin.Icon, skin.Description);
       public static Skin ToModel(this SkinDTO skinDTO) 
            => new Skin(skinDTO.Name, null, skinDTO.Price, skinDTO.Icon,"",skinDTO.Description);
    }
}
