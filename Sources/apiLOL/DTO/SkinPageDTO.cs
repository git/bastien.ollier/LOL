﻿namespace apiLOL.DTO
{
    public class SkinPageDTO
    {
        public IEnumerable<SkinDTO> Data { get; set; }

        public int Index { get; set; }

        public int Count { get; set; }

        public int TotalCount { get; set; }
    }
}
